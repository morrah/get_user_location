	(function(){
	/*
	Purpose:
	by html5 geolocation get user current position
	by Google Maps geocoder get user postcode
	by own server script through AJAX get location of the nearest company office and show it on the Google Map
	show user the prompt form to give user enter another postcode and search nearest office again
	save office postcode, office ID and wheather or not make html5 geolocation to use it in other pages of your site
	
	It does not require neather Google Maps JS API connected nor <div> container in Your HTML to put the Google map, but if there are JS API and/or container, it use them doing no additional connections or container.
	
	There are several settings to give this script some flexibility. All them are described in the comments to settings object at the begin og code.
	
	This script REQUIRES connected jQuery!
	
	Author: Helen Lazar
	Contacts:
	webdepo@list.ru, 
	helen.lazar.77@gmail.com, 
	oDesk: https://www.odesk.com/o/profiles/users/_~012de4ed47ea5f6edb/ (Helen Lazar), 
	LinkedIn: https://www.linkedin.com/profile/view?id=337786491 (Елена Лазарь)
	bitbucket: https://bitbucket.org/morrah
	*/
	var settings={
		show_user_pos:false,/*wheather show user's position detected by html5 geolocation or no*/
		/*To place the prompt form into certain div write its id
		To place the map into certain div write its id
		So if You want to place like 
		<div id="geolocation">
			<div id="askforpostcode" class="hide"><input.../></div>
			<div id="the-map"><google map...> </div>
		</div>
		write 
		<div id="geolocation" style="width:100%; height:100%">
			<div id="askforpostcode"></div>
			<div id="the-map" style="width:100%; height:100%"></div>
		</div>
		into HTML and write
		map_div_id:"the-map",
		prompt_form_id:"askforpostcode",
		...
		into settings
		
		Please remember to write the divs style properties width and height in your HTML or CSS, it's necessary to display Google Maps correctly
		*/
		map_div_id:"the-map",/*div id to place Google map*/
		prompt_form_id:"askforpostcode",/*div id to put prompt form into it*/
		map_api_key:"",/*Your Google Maps API key*/
		map_defaults:{/*default settings of the Google map*/
			center_lat:-34.397,
			center_lng:150.644,
			zoom:8,
			marker_title:"The nearest location",
			user_loc_info:"Your location found using HTML5.",/*Title of infowindow of user current place*/
			user_loc_marker:"Your current position",/*Title of marker of user current place*/
			marker_icon:"http://maps.google.com/mapfiles/ms/icons/blue-dot.png"/*thr URL of map markers icon*/
		},
		ajax_url:"/ajax/getlocation.php",/*our server script url to retreive json data by the postcode*/
		/*data format:
		data={"oID":"7","oOffice":"Brisbane","oPostcode":"4059","oPhone":"(07) 3834 4777","oAddress":"43 Musgrave Rd","oCity":"Red Hill","oState":"QLD","oLat":"-27.457932","oLong":"153.012283","querypostcode":"4059"};
		*/
		messages:{/*Enter your own message texts and/or localize your form*/
			get_location_error:"Could not detect nearest office location. Please enter another postcode and tyy again.",/*Error of our server script*/
			get_location_auto_failure:"We could not detect your location automatically. Please enter your Australian postcode.",/*Our server script could not find office*/
			prompt_form_title:"Find Your nearest location",/*The title of prompt form*/
			input_label:"Find your nearest location: Enter your postcode",/*the label of postcode input in the prompt form*/
			input_placeholder:"Ex.: 2000",/*the placeholder of postcode input in the prompt form*/
			search_btn_text:"Search",/*search button text in the prompt form*/
			gl_not_supp:"Geolocation is not supported by this browser."/*If html5 geolocation is not supported in the browser*/
		},
		console_log:false/*wheather or not log into console*/
	};
	
	var user_pos, office_pos, postcode;
	var map,data,marker,markerup,get_postcode;
	var scr=$( "<script/>" ).appendTo( "body" );
	var gl_req='';//if the geolocation request was ignored
	
	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}

	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1);
			if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
		}
		return "";
	}
	
	function deleteAllCookies(){
		var ourcookies=['ql_req','postcode','offID'];
		for(var i=0;i<ourcookies.length;i++){
			setCookie(ourcookies[i],'',1);
			if(settings.console_log){
			console.log('the cookie '+ourcookies[i]+' is deleted');
			}
			var c=getCookie(ourcookies[i]);
			if(settings.console_log){
			console.log('the cookie '+ourcookies[i]+' value is:');
			console.dir(c);
			}
		}
	}
	window.deleteAllCookies=function(){deleteAllCookies()};
	function initialize() {
		var mapOptions = {
			center: new google.maps.LatLng(settings.map_defaults.center_lat,settings.map_defaults.center_lng),
			zoom: settings.map_defaults.zoom,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		map = new google.maps.Map(document.getElementById(settings.map_div_id),mapOptions);
		$(window).on('resize', function() {
			var center = map.getCenter();
			google.maps.event.trigger(map, 'resize');
			map.setCenter(center);
		});
	}
	function showNearestLocation(event,obj){
		if(settings.console_log){
		console.log( 'do showNearestLocation()' );
		console.log( 'event=' );
		console.dir( event );
		console.log( 'obj=' );
		console.dir( obj );
		console.log('the searching input:');
		console.dir($($(obj).find('#'+$(obj).attr('id')+'_'+'postcode').get(0)));
		console.log('the searching input value:');
		console.dir($($(obj).find('#'+$(obj).attr('id')+'_'+'postcode').get(0)).val());
		}
		//if(!postcode){
			postcode=$($(obj).find('#'+$(obj).attr('id')+'_'+'postcode').get(0)).val();
		//}
		if((isEmpty(postcode))||(typeof(postcode)=='object')||(typeof(postcode)=='array')){
			postcode='';
		}
		if(settings.console_log){
		console.log('postcode: '+postcode);
		}
		//scr.load("http://infinitypower.com.au/ajax/getlocation.php",alert("!"));
		$.ajax({
			// the URL for the request
			url: settings.ajax_url,
		 
			// the data to send (will be converted to a query string)
			data: {
				postcode: postcode
				//postcode: $($('#ajax_searchform').find('#postcode').get(0)).val()
			},
		 
			// whether this is a POST or GET request
			type: "POST",
		 
			// the type of data we expect back
			dataType : "json",
		 
			// code to run if the request succeeds;
			// the response is passed to the function
			success: function( json ) {
				if(settings.console_log){
				console.log( 'do success of showNearestLocation()' );
				}
				data=json;
				if(settings.console_log){
				console.log( "data=" );
				console.dir( json );
				}
				//$( "<div class=\"content\"/>").html( json.html ).appendTo( "body" );
			},
		 
			// code to run if the request fails; the raw request and
			// status codes are passed to the function
			error: function( xhr, status, errorThrown ) {
				if(settings.console_log){
				console.log( 'do error of showNearestLocation()' );
				}
				//alert( "Sorry, there was a problem!" );
				$('div.gl_prompt div#gl_prompt_msg').addClass('warn').text(settings.messages.get_location_error);
				if(settings.console_log){
				console.log( "Error: " + errorThrown );
				console.log( "Status: " + status );
				console.dir( xhr );
				}
			},
		 
			// code to run regardless of success or failure
			complete: function( xhr, status ) {
				if(settings.console_log){
				console.log( 'do complete of showNearestLocation()' );
				}
				if(!map){
					showMap();
				}
				if(data.oLat&&data.oLong){
					//alert( "The request is complete!" );
					office_pos= new google.maps.LatLng(data.oLat,data.oLong);
					$('div.gl_prompt div#gl_prompt_msg').removeClass('warn').text('');
					if(!marker){
						marker = new google.maps.Marker({
							icon: settings.map_defaults.marker_icon,
							position: office_pos,
							map: map,
							title: settings.map_defaults.marker_title+((data.oOffice)?(': '+data.oOffice):'')+((data.oAddress)?(', '+data.oAddress+((data.oCity)?(' '+data.oCity):'')):'')+((data.oState)?(', '+data.oState):'')+((data.oPostcode)?(': '+data.oPostcode):'')
						  });
					}
					else{
						marker.setPosition(office_pos);
					}
					map.setCenter(office_pos);
					map.panTo(office_pos);
					if(settings.show_user_pos){
						if(user_pos&&office_pos&&(user_pos!==office_pos)){
							var south=((user_pos.lat()<=office_pos.lat())?user_pos.lat():office_pos.lat());
							var north=((user_pos.lat()<=office_pos.lat())?office_pos.lat():user_pos.lat());
							var west=((user_pos.lng()<=office_pos.lng())?user_pos.lng():office_pos.lng());
							var east=((user_pos.lng()<=office_pos.lng())?office_pos.lng():user_pos.lng());
							var bounds=new google.maps.LatLngBounds(new google.maps.LatLng(south,west), new google.maps.LatLng(north,east));
							if(settings.console_log){
							console.log('bounds=');
							console.dir(bounds);
							}
							//map.setZoom(0);
							//map.panToBounds(bounds);/*sw?:LatLng, ne?:LatLng*/
							map.fitBounds(bounds);
							//map.panToBounds(new google.maps.LatLngBounds(new google.maps.LatLng(-30,82), new google.maps.LatLng(55,153)));/*sw?:LatLng, ne?:LatLng*/
						}
						else{
							map.setZoom(13);
						}
					}
					else{
						map.setZoom(13);
					}
				}
				else{
					$('div.gl_prompt div#gl_prompt_msg').addClass('warn').text(settings.messages.get_location_auto_failure);
					$('div.gl_prompt').find('form').show();
				}
				if(data.oPostcode)
				setCookie('postcode', data.querypostcode, 1);
				if(data.oPostcode)
				setCookie('offID', data.oID, 1);
			},
			
			cache: false,
			
			timeout: 5000/*,
			
			crossDomain: true*/
		});
	}
	function createPromptForm(){
		if(!isEmpty(settings.prompt_form_id)&&($("#"+settings.prompt_form_id).length>0)){
			$( "<div class=\"gl_prompt\" id=\"gl_prompt\" />").appendTo( "#"+settings.prompt_form_id );
		}
		else{
		$( "<div class=\"gl_prompt\" id=\"gl_prompt\" />").appendTo( "body" );
		}
		$( "<a id=\"gl_prompt_toggle\">"+settings.messages.prompt_form_title+"</a>").appendTo( "div.gl_prompt" );
		$( "<div id=\"gl_prompt_msg\"></div>").appendTo( "div.gl_prompt" );
		$( "<form method=\"POST\" id=\"gl_prompt_form\" style=\"display:none;\"/>").appendTo( "div.gl_prompt" );
		$( "<label for=\"gl_prompt_form_postcode\">"+settings.messages.input_label+"</label/>").appendTo( "form#gl_prompt_form" );
		$( "<input type=\"text\" name=\"postcode\" id=\"gl_prompt_form_postcode\" placeholder=\""+settings.messages.input_placeholder+"\"/>").appendTo( "form#gl_prompt_form" );
		$( "form#gl_prompt_form input #gl_prompt_form_postcode" ).val(getCookie('postcode'));
		$( "<button id=\"gl_prompt_search\" name=\"search\">"+settings.messages.search_btn_text+"</button>").appendTo( "form#gl_prompt_form" );
		$('div.gl_prompt a#gl_prompt_toggle').on('click',function(e){
			$(this).parent('div.gl_prompt').find('form').toggle();
		});
	}
			
	function takepostcodefrominquiry(){
		if(settings.console_log){
		console.log('do takepostcodefrominquiry()');
		console.log('div#enquiryform form input#postcode'+':');
		console.log($('div#enquiryform form input#postcode'));
		}
		$('div#enquiryform form input#postcode').val($('form#gl_prompt_form input#gl_prompt_form_postcode').val());
		$('div#enquiryform form input#hiddenPostcode').val($('form#gl_prompt_form input#gl_prompt_form_postcode').val());
		$('div#enquiryform form input#offID').val(getCookie('offID'));
	}

	function getLocation() {
		if(settings.console_log){
		console.log('do getLocation');
		}
		if(!get_postcode){
			get_postcode=false;
		}
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position){
				setCookie('ql_reg', '', 1);	
				user_pos=new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
				if(settings.console_log){
				console.log("Latitude: " + position.coords.latitude + ", Longitude: " + position.coords.longitude);	
				console.log('user_pos=');
				console.dir(user_pos);
				console.log('settings=');
				console.dir(settings);
				console.log('settings.show_user_pos=');
				console.dir(settings.show_user_pos);
				}
				if(settings.show_user_pos){
					if(settings.console_log){
					console.log('map=');
					console.dir(map);
					console.log('!map=');
					console.dir(!map);
					}
					if(!map){
						showMap();
					}
					if(settings.console_log){
					console.log('after showMap: map=');
					console.dir(map);
					}
					if(map.getCenter){
						var infowindow = new google.maps.InfoWindow({
							map: map,
							position: user_pos,
							content: settings.map_defaults.user_loc_info
						});
						if(!markerup)
							var markerup = new google.maps.Marker({
								icon: settings.map_defaults.marker_icon,
								position: user_pos,
								map: map,
								title: settings.map_defaults.user_loc_marker
							  });
						else
							markerup.setPosition(user_pos);
						map.setCenter(user_pos);
						map.panTo(user_pos);
						map.setZoom(13);
					}
				}
				if(settings.console_log){
				console.log('get_postcode=');
				console.dir(get_postcode);
				}
				if(get_postcode){
					postcode = getPostCodeFromUserPos();
					if(settings.console_log){
					console.log('postcode=');
					console.dir(postcode);
					}
				}
			},showError);
		} else { 
			console.log(settings.messages.gl_not_supp);
			setCookie('ql_reg', '1', 1);
		}
	}

	/*function showPosition(position,get_postcode) {
		setCookie('ql_reg', '', 1);	
		console.log("Latitude: " + position.coords.latitude + ", Longitude: " + position.coords.longitude);	
		user_pos=new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
		if(settings.show_user_pos){
			if(!map){
				showMap();
			}
			if(map.getCenter){
				var infowindow = new google.maps.InfoWindow({
					map: map,
					position: user_pos,
					content: 'Your location found using HTML5.'
				});
				if(!markerup)
					var markerup = new google.maps.Marker({
						position: user_pos,
						map: map,
						title: 'Your current position'
					  });
				else
					markerup.setPosition(user_pos);
				map.setCenter(user_pos);
				map.panTo(user_pos);
				map.setZoom(13);
			}
		}
		if(get_postcode){
			postcode = getPostCodeFromUserPos();
		}
	}*/

	function showError(error) {
		switch(error.code) {
			case error.PERMISSION_DENIED:
				console.log("User denied the request for Geolocation.")
				setCookie('ql_reg', '1', 1);
				break;
			case error.POSITION_UNAVAILABLE:
				console.log("Location information is unavailable.")
				break;
			case error.TIMEOUT:
				console.log("The request to get user location timed out.")
				break;
			case error.UNKNOWN_ERROR:
				console.log("An unknown error occurred.")
				break;
		}
		$('div.gl_prompt div#gl_prompt_msg').addClass('warn').text('HTML5 geolocation could not detect your location automatically. Please enter your postcode.');
		$('div.gl_prompt').find('form').show();
	}
	/*function connectGM(){
		console.log('do connectGM()');
		try{
			console.log('google.maps existence try');
			if((google)&&(typeof(google)=='object')&&(google.maps)){
				console.log('google.maps already connected');
			}
		}
		catch(e){
			console.log('there is no google.maps');
			console.dir(e);
			var ga = document.createElement("script");
			ga.type = "text/javascript";
			ga.async = true;
			ga.src = ("https:" == document.location.protocol ? "https://" : "http://") + "maps.googleapis.com/maps/api/js?sensor=false&v=3.exp&callback=getPostalCode";
			var scr = document.getElementsByTagName("script")[0]; scr.parentNode.insertBefore(ga, scr);
		}
		finally{
			console.dir(google);
		}
	}*/
	function showMap(){
		//connectGM();
		if(settings.console_log){
		console.log('do showMap()');
		}
		if(!document.getElementById(settings.map_div_id)){
			$("<div id=\""+settings.map_div_id+"\"></div>").appendTo('body');
		}
		initialize();
	}
	function getPostCodeFromUserPos(){
		if(settings.console_log){
		console.log('do getPostCodeFromUserPos()');
		}
		var  postcode=false;
		if(user_pos){
			//connectGM();
			var geocoder = new google.maps.Geocoder();
			geocoder.geocode({'latLng': user_pos}, function(results, status){
				if (status == google.maps.GeocoderStatus.OK) {
					var i, j, result, types;
					for (i = 0; i < results.length; i++) {
						result = results[i];
						types = result.types;
						for (j = 0; j < types.length; j++) {
							if (types[j] === 'postal_code') {
								if (result.long_name === undefined) {
									results = result.address_components;
									i = -1;
								}
								else {
									postcode = result.long_name;
									if(postcode){
										$('form#gl_prompt_form input#gl_prompt_form_postcode').val(postcode);
										$('#gl_prompt_form').trigger('submit');
									}
								}
								break;
							}
						}
					}
				}
			});
		}
		return postcode;
	}
	function isEmpty(arg){
		if((arg===undefined)||(arg===false)||(arg.trim()=='')){
			return true;
		}	
	}
	function getPostalCode(){
		if(settings.console_log){
		console.log('do getPostalCode');
		}
		postcode=getCookie('postcode');
		if(isEmpty(postcode)){
			postcode=$('form#gl_prompt_form input#gl_prompt_form_input').val();
		}
		if(isEmpty(postcode)){
			postcode=$('div#enquiryform form input#postcode').val();
		}
		if(!isEmpty(postcode)){
			$('form#gl_prompt_form input#gl_prompt_form_postcode').val(postcode);
			$('#gl_prompt_form').trigger('submit');
		}
		else{
			gl_req=getCookie('gl_req');
			if(!gl_req){
				get_postcode=true;
				getLocation();
				if(!isEmpty(postcode)){
					$('form#gl_prompt_form input#gl_prompt_form_postcode').val(postcode);
					$('#gl_prompt_form').trigger('submit');
				}
			}
		}
	}
	window.getPostalCode=function(){getPostalCode();}
	$(document).ready(function(){
		createPromptForm();
		$('#gl_prompt_form').on('submit',function(e){
			showNearestLocation(e,this);
			takepostcodefrominquiry();
			return false;
		});
		try{
			if(settings.console_log){
			console.log('google.maps existence try');
			}
			if((google)&&(typeof(google)=='object')&&(google.maps)){
				if(settings.console_log){
				console.log('google.maps already connected');
				}
				getPostalCode();
			}
		}
		catch(e){
				if(settings.console_log){
					console.log('there is no google.maps');
					console.dir(e);
				}
				var script = document.createElement("script");
				script.type = "text/javascript";
				script.src = ("https:" == document.location.protocol ? "https://" : "http://") + "maps.googleapis.com/maps/api/js?"+(!isEmpty(settings.map_api_key)?"key="+settings.map_api_key+"&":"")+"sensor=false&v=3.exp&callback=getPostalCode";
				document.body.appendChild(script);
		}
		
	});
	})();