<?php
	header('Content-Type: application/json');
	$postcode = strval(htmlspecialchars($_POST['postcode']));
	switch($postcode){
		case "2000":
			$data='{"oID":"10","oOffice":"Coffs Harbour","oPostcode":"2450","oPhone":"(02) 6650 0408","oAddress":"90/84 Industrial Dr","oCity":"North Boambee Valley","oState":"NSW","oLat":"-30.316610","oLong":"153.084839","querypostcode":"2000","received_$postcode":"'.$postcode.'"}';
			break;
		case "4059":
			$data='{"oID":"7","oOffice":"Brisbane","oPostcode":"4059","oPhone":"(07) 3834 4777","oAddress":"43 Musgrave Rd","oCity":"Red Hill","oState":"QLD","oLat":"-27.457932","oLong":"153.012283","querypostcode":"4059","received_$postcode":"'.$postcode.'"}';
			break;
		case "4000":
			$data='{"oID":"7","oOffice":"Brisbane","oPostcode":"4059","oPhone":"(07) 3834 4777","oAddress":"43 Musgrave Rd","oCity":"Red Hill","oState":"QLD","oLat":"-27.457932","oLong":"153.012283","querypostcode":"4000","received_$postcode":"'.$postcode.'"}';
			break;
		default:
			$data='{"querypostcode":"","received_postcode":"'.$postcode.'"}';
	}
	echo $data;
?>