Author: Helen Lazar
Contacts:
webdepo@list.ru, 
helen.lazar.77@gmail.com, 
oDesk: https://www.odesk.com/o/profiles/users/_~012de4ed47ea5f6edb/ (Helen Lazar), 
LinkedIn: https://www.linkedin.com/profile/view?id=337786491 (Елена Лазарь)
bitbucket: https://bitbucket.org/morrah
	
This script purposed to get user location through the html5 geolocation, get user postcode through the Google Maps geocoder using that position, and then perform AJAX request to the certain URL in the same server to retreive JSON data containing information about nearest office.

This script REQUIRES connected jQuery!
	
It has some settings to add more flexibility. All the settings are described in the code, but now I repeat the description of them here:
var settings={
		show_user_pos:false,/*wheather show user's position detected by html5 geolocation or no*/
		/*To place the prompt form into certain div write its id
		To place the map into certain div write its id
		So if You want to place like 
		<div id="geolocation">
			<div id="askforpostcode" class="hide"><input.../></div>
			<div id="the-map"><google map...> </div>
		</div>
		write 
		<div id="geolocation" style="width:100%; height:100%">
			<div id="askforpostcode"></div>
			<div id="the-map" style="width:100%; height:100%"></div>
		</div>
		into HTML and write
		map_div_id:"the-map",
		prompt_form_id:"askforpostcode",
		...
		into settings
		
		Please remember to write the divs style properties width and height in your HTML or CSS, it's necessary to display Google Maps correctly
		*/
		map_div_id:"the-map",/*div id to place Google map*/
		prompt_form_id:"askforpostcode",/*div id to put prompt form into it*/
		map_api_key:"",/*Your Google Maps API key*/
		map_defaults:{/*default settings of the Google map*/
			center_lat:-34.397,
			center_lng:150.644,
			zoom:8,
			marker_title:"The nearest location",
			user_loc_info:"Your location found using HTML5.",/*Title of infowindow of user current place*/
			user_loc_marker:"Your current position",/*Title of marker of user current place*/
			marker_icon:"http://maps.google.com/mapfiles/ms/icons/blue-dot.png"/*thr URL of map markers icon*/
		},
		ajax_url:"/ajax/getlocation.php",/*our server script url to retreive json data by the postcode*/
		/*data format:
		data={"oID":"7","oOffice":"Brisbane","oPostcode":"4059","oPhone":"(07) 3834 4777","oAddress":"43 Musgrave Rd","oCity":"Red Hill","oState":"QLD","oLat":"-27.457932","oLong":"153.012283","querypostcode":"4059"};
		*/
		messages:{/*Enter your own message texts and/or localize your form*/
			get_location_error:"Could not detect nearest office location. Please enter another postcode and tyy again.",/*Error of our server script*/
			get_location_auto_failure:"We could not detect your location automatically. Please enter your Australian postcode.",/*Our server script could not find office*/
			prompt_form_title:"Find Your nearest location",/*The title of prompt form*/
			input_label:"Find your nearest location: Enter your postcode",/*the label of postcode input in the prompt form*/
			input_placeholder:"Ex.: 2000",/*the placeholder of postcode input in the prompt form*/
			search_btn_text:"Search",/*search button text in the prompt form*/
			gl_not_supp:"Geolocation is not supported by this browser."/*If html5 geolocation is not supported in the browser*/
		},
		console_log:true
	};